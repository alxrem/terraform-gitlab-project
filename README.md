Terraform Gitlab Project Module
===============================

This module helps to configure Gitlab project with some of associated resources
like protected branches, variables etc.

<!-- BEGIN_TF_DOCS -->
## Requirements

The following requirements are needed by this module:

- <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) (~> 1.3)

- <a name="requirement_gitlab"></a> [gitlab](#requirement\_gitlab) (~> 17.1)

- <a name="requirement_time"></a> [time](#requirement\_time) (~> 0.4)

## Required Inputs

The following input variables are required:

### <a name="input_name"></a> [name](#input\_name)

Description: The name of the project.

Type: `string`

## Optional Inputs

The following input variables are optional (have default values):

### <a name="input_access_tokens"></a> [access\_tokens](#input\_access\_tokens)

Description: Project access tokens.

Key of map is the name to describe the project access token.

Values describe the attributes of the access tokens.

- scopes &mdash; The scope for the project access token. It determines  
  the actions which can be performed when authenticating with this token.  
  Valid values are: `api`, `read_api`, `read_registry`, `write_registry`,
  `read_repository`, `write_repository`, `create_runner`.
- expires (optional) &mdash; Time the token will expire it, YYYY-MM-DD format;  
  by default one year from date of creation.
- access\_level (optional) &mdash; The access level for the project access token.  
  Valid values are: `no one`, `minimal`, `guest`, `reporter`, `developer`,
  `maintainer`, `owner`, `master`. Default is `maintainer`.

Type:

```hcl
map(object({
    scopes       = set(string)
    expires_at   = optional(string)
    access_level = optional(string)
  }))
```

Default: `{}`

### <a name="input_allow_merge_on_skipped_pipeline"></a> [allow\_merge\_on\_skipped\_pipeline](#input\_allow\_merge\_on\_skipped\_pipeline)

Description: Set to true if you want to treat skipped pipelines as if they finished with success.

Type: `bool`

Default: `null`

### <a name="input_analytics_access_level"></a> [analytics\_access\_level](#input\_analytics\_access\_level)

Description: Set the analytics access level. Valid values are disabled, private, enabled.

Type: `string`

Default: `null`

### <a name="input_approvals_before_merge"></a> [approvals\_before\_merge](#input\_approvals\_before\_merge)

Description: Number of merge request approvals required for merging (EE only).

Type: `number`

Default: `null`

### <a name="input_archive_on_destroy"></a> [archive\_on\_destroy](#input\_archive\_on\_destroy)

Description: Set to true to archive the project instead of deleting on destroy.

Type: `bool`

Default: `null`

### <a name="input_archived"></a> [archived](#input\_archived)

Description: Whether the project is in read-only mode (archived). Repositories can be archived/unarchived by toggling this parameter

Type: `bool`

Default: `null`

### <a name="input_auto_cancel_pending_pipelines"></a> [auto\_cancel\_pending\_pipelines](#input\_auto\_cancel\_pending\_pipelines)

Description: Auto-cancel pending pipelines.

Type: `bool`

Default: `null`

### <a name="input_auto_devops_deploy_strategy"></a> [auto\_devops\_deploy\_strategy](#input\_auto\_devops\_deploy\_strategy)

Description: Auto Deploy strategy. Valid values are continuous, manual, timed\_incremental.

Type: `string`

Default: `null`

### <a name="input_auto_devops_enabled"></a> [auto\_devops\_enabled](#input\_auto\_devops\_enabled)

Description: Enable Auto DevOps for this project.

Type: `bool`

Default: `null`

### <a name="input_autoclose_referenced_issues"></a> [autoclose\_referenced\_issues](#input\_autoclose\_referenced\_issues)

Description: Set whether auto-closing referenced issues on default branch.

Type: `bool`

Default: `null`

### <a name="input_avatar"></a> [avatar](#input\_avatar)

Description: A local path to the avatar image to upload.

Type: `string`

Default: `null`

### <a name="input_avatar_hash"></a> [avatar\_hash](#input\_avatar\_hash)

Description:   The hash of the avatar image. this is used to trigger an update of the avatar.  
  If it's not given, but an avatar is given, the avatar will be updated each time.

Type: `string`

Default: `null`

### <a name="input_build_git_strategy"></a> [build\_git\_strategy](#input\_build\_git\_strategy)

Description:  The Git strategy. Defaults to fetch. Valid values are "clone", "fetch".

Type: `string`

Default: `null`

### <a name="input_build_timeout"></a> [build\_timeout](#input\_build\_timeout)

Description: The maximum amount of time, in seconds, that a job can run.

Type: `number`

Default: `null`

### <a name="input_builds_access_level"></a> [builds\_access\_level](#input\_builds\_access\_level)

Description: Set the builds access level. Valid values are "disabled", "private", "enabled".

Type: `string`

Default: `null`

### <a name="input_ci_config_path"></a> [ci\_config\_path](#input\_ci\_config\_path)

Description: Custom Path to CI config file.

Type: `string`

Default: `null`

### <a name="input_ci_default_git_depth"></a> [ci\_default\_git\_depth](#input\_ci\_default\_git\_depth)

Description: Default number of revisions for shallow cloning.

Type: `number`

Default: `null`

### <a name="input_ci_forward_deployment_enabled"></a> [ci\_forward\_deployment\_enabled](#input\_ci\_forward\_deployment\_enabled)

Description: When a new deployment job starts, skip older deployment jobs that are still pending.

Type: `bool`

Default: `null`

### <a name="input_ci_separated_caches"></a> [ci\_separated\_caches](#input\_ci\_separated\_caches)

Description: Use separate caches for protected branches.

Type: `bool`

Default: `null`

### <a name="input_container_expiration_policy"></a> [container\_expiration\_policy](#input\_container\_expiration\_policy)

Description: Set the image cleanup policy for this project.

Type:

```hcl
object({
    cadence           = optional(string)
    enabled           = optional(bool, true)
    keep_n            = optional(number)
    name_regex        = optional(string)
    name_regex_delete = optional(string)
    name_regex_keep   = optional(string)
    older_than        = optional(string)
  })
```

Default: `null`

### <a name="input_container_registry_access_level"></a> [container\_registry\_access\_level](#input\_container\_registry\_access\_level)

Description: Set visibility of container registry, for this project. Valid values are "disabled", "private" or "enabled".

Type: `string`

Default: `null`

### <a name="input_container_registry_enabled"></a> [container\_registry\_enabled](#input\_container\_registry\_enabled)

Description: Enable container registry for the project.

Type: `bool`

Default: `null`

### <a name="input_default_branch"></a> [default\_branch](#input\_default\_branch)

Description: The default branch for the project.

Type: `string`

Default: `null`

### <a name="input_description"></a> [description](#input\_description)

Description: Description of project.

Type: `string`

Default: `null`

### <a name="input_emails_enabled"></a> [emails\_enabled](#input\_emails\_enabled)

Description: Enable email notifications.

Type: `bool`

Default: `null`

### <a name="input_environments_access_level"></a> [environments\_access\_level](#input\_environments\_access\_level)

Description: Set the environments access level. Valid values are "disabled", "private" or "enabled".

Type: `string`

Default: `null`

### <a name="input_external_authorization_classification_label"></a> [external\_authorization\_classification\_label](#input\_external\_authorization\_classification\_label)

Description: The classification label for the project.

Type: `string`

Default: `null`

### <a name="input_feature_flags_access_level"></a> [feature\_flags\_access\_level](#input\_feature\_flags\_access\_level)

Description: Set the feature flags level. Valid values are "disabled", "private" or "enabled".

Type: `string`

Default: `null`

### <a name="input_forked_from_project_id"></a> [forked\_from\_project\_id](#input\_forked\_from\_project\_id)

Description:   The id of the project to fork. During create the project is forked  
  and during an update the fork relation is changed.

Type: `number`

Default: `null`

### <a name="input_forking_access_level"></a> [forking\_access\_level](#input\_forking\_access\_level)

Description: Set the feature flags level. Valid values are "disabled", "private" or "enabled".

Type: `string`

Default: `null`

### <a name="input_group_with_project_templates_id"></a> [group\_with\_project\_templates\_id](#input\_group\_with\_project\_templates\_id)

Description:   For group-level custom templates, specifies ID of group from which  
  all the custom project templates are sourced. Leave empty  
  for instance-level templates. Requires use\_custom\_template to be true
  (enterprise edition).

Type: `number`

Default: `null`

### <a name="input_import_url"></a> [import\_url](#input\_import\_url)

Description:   Git URL to a repository to be imported. Together with `mirror = true`  
  it will setup a Pull Mirror. This can also be used together  
  with `forked_from_project_id` to setup a Pull Mirror for a fork.  
  The fork takes precedence over the import. Make sure to provide  
  the credentials in `import_url_username` and `import_url_password`.  
  GitLab never returns the credentials, thus the provider cannot detect  
  configuration drift in the credentials. They can also not be imported  
  using terraform import. See the examples section for how to properly use it.

Type: `string`

Default: `null`

### <a name="input_import_url_password"></a> [import\_url\_password](#input\_import\_url\_password)

Description:   The password for the `import_url`. The value of this field is used  
  to construct a valid `import_url` and is only related to the provider.  
  This field cannot be imported using terraform import.

Type: `string`

Default: `null`

### <a name="input_import_url_username"></a> [import\_url\_username](#input\_import\_url\_username)

Description:   The username for the `import_url`. The value of this field is used  
  to construct a valid `import_url` and is only related to the provider.  
  This field cannot be imported using terraform import.

Type: `string`

Default: `null`

### <a name="input_infrastructure_access_level"></a> [infrastructure\_access\_level](#input\_infrastructure\_access\_level)

Description: Set the infrastructure access level. Valid values are "disabled", "private" or "enabled".

Type: `string`

Default: `null`

### <a name="input_initialize_with_readme"></a> [initialize\_with\_readme](#input\_initialize\_with\_readme)

Description: Create main branch with first commit containing a README.md file.

Type: `string`

Default: `null`

### <a name="input_integration_pipelines_email"></a> [integration\_pipelines\_email](#input\_integration\_pipelines\_email)

Description: The integration with Pipeline Emails Service.

- recipients &mdash; email addresses where notifications are sent.
- branches\_to\_be\_notified (optional) &mdash; Branches to send notifications for. Valid options are all, default, protected, and default\_and\_protected. Default is default
- notify\_only\_broken\_pipelines (optional) &mdash; Notify only broken pipelines. Default is true.

Type:

```hcl
object({
    recipients                   = set(string)
    notify_only_broken_pipelines = optional(bool)
    branches_to_be_notified      = optional(string)
  })
```

Default: `null`

### <a name="input_integration_telegram"></a> [integration\_telegram](#input\_integration\_telegram)

Description:   The integration with Telegram service.

  - token &mdash; The Telegram bot token.
  - room &mdash; Unique identifier for the target chat or the username of the target channel (in the format @channelusername).

  - notify\_only\_broken\_pipelines &mdash; Send notifications for broken pipelines.
  - branches\_to\_be\_notified &mdash; Branches to send notifications for (introduced in GitLab 16.5). Update of this attribute was not supported before Gitlab 16.11 due to API bug. Valid options are all, default, protected, default\_and\_protected.
  - push\_events &mdash; Enable notifications for push events.
  - issues\_events &mdash; Enable notifications for issues events.
  - confidential\_issues\_events &mdash; Enable notifications for confidential issues events.
  - merge\_requests\_events &mdash; Enable notifications for merge requests events.
  - tag\_push\_events &mdash; Enable notifications for tag push events.
  - note\_events &mdash; Enable notifications for note events.
  - confidential\_note\_events &mdash; Enable notifications for confidential note events.
  - pipeline\_events &mdash; Enable notifications for pipeline events.
  - wiki\_page\_events &mdash; Enable notifications for wiki page events.

Type:

```hcl
object({
    token = string
    room  = string

    notify_only_broken_pipelines = optional(bool, true)
    branches_to_be_notified      = optional(string, "default")
    push_events                  = optional(bool, false)
    issues_events                = optional(bool, false)
    confidential_issues_events   = optional(bool, false)
    merge_requests_events        = optional(bool, false)
    tag_push_events              = optional(bool, false)
    note_events                  = optional(bool, false)
    confidential_note_events     = optional(bool, false)
    pipeline_events              = optional(bool, false)
    wiki_page_events             = optional(bool, false)
  })
```

Default: `null`

### <a name="input_issues_access_level"></a> [issues\_access\_level](#input\_issues\_access\_level)

Description: Set the issues access level. Valid values are "disabled", "private" or "enabled".

Type: `string`

Default: `null`

### <a name="input_issues_enabled"></a> [issues\_enabled](#input\_issues\_enabled)

Description: Enable issue tracking for the project.

Type: `bool`

Default: `null`

### <a name="input_issues_template"></a> [issues\_template](#input\_issues\_template)

Description: Sets the template for new issues in the project.

Type: `string`

Default: `null`

### <a name="input_keep_latest_artifact"></a> [keep\_latest\_artifact](#input\_keep\_latest\_artifact)

Description: Disable or enable the ability to keep the latest artifact for this project.

Type: `bool`

Default: `null`

### <a name="input_lfs_enabled"></a> [lfs\_enabled](#input\_lfs\_enabled)

Description: Enable LFS for the project.

Type: `bool`

Default: `null`

### <a name="input_merge_commit_template"></a> [merge\_commit\_template](#input\_merge\_commit\_template)

Description: Template used to create merge commit message in merge requests.

Type: `string`

Default: `null`

### <a name="input_merge_method"></a> [merge\_method](#input\_merge\_method)

Description: Set the merge method. Valid values are "merge", "rebase\_merge" or "ff".

Type: `string`

Default: `null`

### <a name="input_merge_requests_enabled"></a> [merge\_requests\_enabled](#input\_merge\_requests\_enabled)

Description: Enable merge requests for the project.

Type: `bool`

Default: `null`

### <a name="input_merge_requests_template"></a> [merge\_requests\_template](#input\_merge\_requests\_template)

Description: Sets the template for new merge requests in the project.

Type: `string`

Default: `null`

### <a name="input_merge_trains_enabled"></a> [merge\_trains\_enabled](#input\_merge\_trains\_enabled)

Description: Enable or disable merge trains. Requires `merge_pipelines_enabled` to be set to true to take effect.

Type: `bool`

Default: `null`

### <a name="input_mirror"></a> [mirror](#input\_mirror)

Description: Enable project pull mirror.

Type: `bool`

Default: `null`

### <a name="input_mirror_overwrites_diverged_branches"></a> [mirror\_overwrites\_diverged\_branches](#input\_mirror\_overwrites\_diverged\_branches)

Description: Enable overwrite diverged branches for a mirrored project.

Type: `bool`

Default: `null`

### <a name="input_mirror_trigger_builds"></a> [mirror\_trigger\_builds](#input\_mirror\_trigger\_builds)

Description: Enable trigger builds on pushes for a mirrored project.

Type: `bool`

Default: `null`

### <a name="input_monitor_access_level"></a> [monitor\_access\_level](#input\_monitor\_access\_level)

Description: Set the monitor access level. Valid values are "disabled", "private" or "enabled".

Type: `string`

Default: `null`

### <a name="input_mr_default_target_self"></a> [mr\_default\_target\_self](#input\_mr\_default\_target\_self)

Description: For forked projects, target merge requests to this project. If false, the target will be the upstream project.

Type: `bool`

Default: `null`

### <a name="input_namespace_id"></a> [namespace\_id](#input\_namespace\_id)

Description: The namespace (group or user) of the project. Defaults to your user.

Type: `string`

Default: `null`

### <a name="input_only_allow_merge_if_all_discussions_are_resolved"></a> [only\_allow\_merge\_if\_all\_discussions\_are\_resolved](#input\_only\_allow\_merge\_if\_all\_discussions\_are\_resolved)

Description: Set to true if you want allow merges only if all discussions are resolved.

Type: `bool`

Default: `null`

### <a name="input_only_allow_merge_if_pipeline_succeeds"></a> [only\_allow\_merge\_if\_pipeline\_succeeds](#input\_only\_allow\_merge\_if\_pipeline\_succeeds)

Description: Set to true if you want allow merges only if a pipeline succeeds.

Type: `bool`

Default: `null`

### <a name="input_only_mirror_protected_branches"></a> [only\_mirror\_protected\_branches](#input\_only\_mirror\_protected\_branches)

Description: Enable only mirror protected branches for a mirrored project.

Type: `bool`

Default: `null`

### <a name="input_packages_enabled"></a> [packages\_enabled](#input\_packages\_enabled)

Description: Enable packages repository for the project.

Type: `bool`

Default: `null`

### <a name="input_pages_access_level"></a> [pages\_access\_level](#input\_pages\_access\_level)

Description: Enable pages access control. Valid values are "public", "private", "enabled" or "disabled".

Type: `string`

Default: `null`

### <a name="input_path"></a> [path](#input\_path)

Description: The path of the repository.

Type: `string`

Default: `null`

### <a name="input_pipelines_enabled"></a> [pipelines\_enabled](#input\_pipelines\_enabled)

Description: Enable pipelines for the project.

Type: `bool`

Default: `null`

### <a name="input_printing_merge_request_link_enabled"></a> [printing\_merge\_request\_link\_enabled](#input\_printing\_merge\_request\_link\_enabled)

Description: Show link to create/view merge request when pushing from the command line.

Type: `bool`

Default: `null`

### <a name="input_protected_branches"></a> [protected\_branches](#input\_protected\_branches)

Description: The protections of project branches.

- push\_access\_level (optional) &mdash; Access levels allowed to push. Valid values are: `no one`, `developer`, `maintainer`.
- merge\_access\_level (optional) &mdash; Access levels allowed to push and merge. Valid values are: `no one`, `developer`, `maintainer`.
- allow\_force\_push (optional) &mdash; Can be set to `true` to allow users with push access to force push.

Type:

```hcl
map(object({
    push_access_level            = optional(string)
    merge_access_level           = optional(string)
    allow_force_push             = optional(bool)
    code_owner_approval_required = optional(bool)
  }))
```

Default: `{}`

### <a name="input_public_builds"></a> [public\_builds](#input\_public\_builds)

Description: If true, jobs can be viewed by non-project members.

Type: `bool`

Default: `null`

### <a name="input_public_jobs"></a> [public\_jobs](#input\_public\_jobs)

Description:  If true, jobs can be viewed by non-project members.

Type: `bool`

Default: `null`

### <a name="input_push_rules"></a> [push\_rules](#input\_push\_rules)

Description: Push rules for the project

Type:

```hcl
object({
    author_email_regex            = optional(string)
    branch_name_regex             = optional(string)
    commit_committer_check        = optional(bool)
    commit_message_negative_regex = optional(string)
    commit_message_regex          = optional(string)
    deny_delete_tag               = optional(bool)
    file_name_regex               = optional(string)
    max_file_size                 = optional(number)
    member_check                  = optional(bool)
    prevent_secrets               = optional(bool)
    reject_unsigned_commits       = optional(bool)
  })
```

Default: `null`

### <a name="input_releases_access_level"></a> [releases\_access\_level](#input\_releases\_access\_level)

Description: Set the releases access level. Valid values are "disabled", "private" or "enabled".

Type: `string`

Default: `null`

### <a name="input_remove_source_branch_after_merge"></a> [remove\_source\_branch\_after\_merge](#input\_remove\_source\_branch\_after\_merge)

Description: Enable Delete source branch option by default for all new merge requests.

Type: `bool`

Default: `null`

### <a name="input_repository_access_level"></a> [repository\_access\_level](#input\_repository\_access\_level)

Description: Set the repository access level. Valid values are "disabled", "private" or "enabled".

Type: `string`

Default: `null`

### <a name="input_repository_storage"></a> [repository\_storage](#input\_repository\_storage)

Description: Which storage shard the repository is on. (administrator only)

Type: `string`

Default: `null`

### <a name="input_request_access_enabled"></a> [request\_access\_enabled](#input\_request\_access\_enabled)

Description: Allow users to request member access.

Type: `string`

Default: `null`

### <a name="input_requirements_access_level"></a> [requirements\_access\_level](#input\_requirements\_access\_level)

Description: Set the requirements access level. Valid values are "disabled", "private" or "enabled".

Type: `string`

Default: `null`

### <a name="input_resolve_outdated_diff_discussions"></a> [resolve\_outdated\_diff\_discussions](#input\_resolve\_outdated\_diff\_discussions)

Description: Automatically resolve merge request diffs discussions on lines changed with a push.

Type: `bool`

Default: `null`

### <a name="input_restrict_user_defined_variables"></a> [restrict\_user\_defined\_variables](#input\_restrict\_user\_defined\_variables)

Description: Allow only users with the Maintainer role to pass user-defined variables when triggering a pipeline.

Type: `bool`

Default: `null`

### <a name="input_security_and_compliance_access_level"></a> [security\_and\_compliance\_access\_level](#input\_security\_and\_compliance\_access\_level)

Description: Set the security\_and\_compliance access level. Valid values are "disabled", "private" or "enabled".

Type: `string`

Default: `null`

### <a name="input_share_groups"></a> [share\_groups](#input\_share\_groups)

Description: Groups to share project with.

It is a map the group ids to required access level.

Example:

share\_groups = {
  "1197" = "maintainer"
  "217"  = "reporter"
}

Type: `map(string)`

Default: `{}`

### <a name="input_share_groups_by_paths"></a> [share\_groups\_by\_paths](#input\_share\_groups\_by\_paths)

Description: Groups to share project with.

It is a map the full group path to required access level.

Example:

share\_groups = {
  "teams/ops" = "maintainer"
  "teams/qa"  = "reporter"
}

Type: `map(string)`

Default: `{}`

### <a name="input_shared_runners_enabled"></a> [shared\_runners\_enabled](#input\_shared\_runners\_enabled)

Description: Enable shared runners for this project.

Type: `bool`

Default: `null`

### <a name="input_skip_wait_for_default_branch_protection"></a> [skip\_wait\_for\_default\_branch\_protection](#input\_skip\_wait\_for\_default\_branch\_protection)

Description:   If true, the default behavior to wait for the default branch protection  
  to be created is skipped. This is necessary if the current user  
  is not an admin and the default branch protection is disabled on  
  an instance-level. There is currently no known way to determine  
  if the default branch protection is disabled on an instance-level  
  for non-admin users. This attribute is only used during resource creation,  
  thus changes are suppressed and the attribute cannot be imported.

Type: `bool`

Default: `null`

### <a name="input_snippets_access_level"></a> [snippets\_access\_level](#input\_snippets\_access\_level)

Description: Set the snippets access level. Valid values are "disabled", "private" or "enabled".

Type: `string`

Default: `null`

### <a name="input_snippets_enabled"></a> [snippets\_enabled](#input\_snippets\_enabled)

Description: Enable snippets for the project.

Type: `bool`

Default: `null`

### <a name="input_squash_commit_template"></a> [squash\_commit\_template](#input\_squash\_commit\_template)

Description: Template used to create squash commit message in merge requests.

Type: `string`

Default: `null`

### <a name="input_squash_option"></a> [squash\_option](#input\_squash\_option)

Description: Squash commits when merge request. Valid values are "never", "always", "default\_on" or "default\_off". The default value is "default\_off".

Type: `string`

Default: `null`

### <a name="input_suggestion_commit_message"></a> [suggestion\_commit\_message](#input\_suggestion\_commit\_message)

Description: The commit message used to apply merge request suggestions.

Type: `string`

Default: `null`

### <a name="input_tags"></a> [tags](#input\_tags)

Description: The list of tags for a project; put array of tags, that should be finally assigned to a project. Use topics instead.

Type: `set(string)`

Default: `null`

### <a name="input_template_name"></a> [template\_name](#input\_template\_name)

Description: When used without use\_custom\_template, name of a built-in project template. When used with use\_custom\_template, name of a custom project template. This option is mutually exclusive with template\_project\_id.

Type: `string`

Default: `null`

### <a name="input_template_project_id"></a> [template\_project\_id](#input\_template\_project\_id)

Description:   When used with use\_custom\_template, project ID of a custom project template.  
  This is preferable to using template\_name since template\_name  
  may be ambiguous (enterprise edition). This option is mutually exclusive  
  with template\_name. See gitlab\_group\_project\_file\_template to set a project  
  as a template project. If a project has not been set as a template,  
  using it here will result in an error.

Type: `number`

Default: `null`

### <a name="input_topics"></a> [topics](#input\_topics)

Description: The list of topics for the project.

Type: `set(string)`

Default: `null`

### <a name="input_use_custom_template"></a> [use\_custom\_template](#input\_use\_custom\_template)

Description:   Use either custom instance or group (with group\_with\_project\_templates\_id)  
  project template (enterprise edition). ~> When using a custom template,  
  Group Tokens won't work. You must use a real user's Personal Access Token.

Type: `bool`

Default: `null`

### <a name="input_variables"></a> [variables](#input\_variables)

Description: The CI/CD variables of project.

Names of variables are the keys of map.

- value &mdash; The value of the variable.
- description &mdash; The description of the variable.
- environment\_scope &mdash; The environment scope of the variable. Defaults to all environment (`*`). Note that in Community Editions of Gitlab, values other than `*` will cause inconsistent plans.
- masked &mdash; If set to `true`, the value of the variable will be hidden in job logs. The value must meet the masking requirements.
- protected &mdash; If set to `true`, the variable will be passed only to pipelines running on protected branches and tags. Defaults to `false`.
- raw &mdash; Whether the variable is treated as a raw string. Default: `false`. When `true`, variables in the value are not expanded.
- variable\_type &mdash; The type of a variable. Valid values are: `env_var`, `file`. Default is `env_var`.

Type:

```hcl
map(object({
    value             = string
    description       = optional(string)
    environment_scope = optional(string)
    masked            = optional(bool)
    protected         = optional(bool)
    raw               = optional(bool)
    variable_type     = optional(string)
  }))
```

Default: `{}`

### <a name="input_visibility_level"></a> [visibility\_level](#input\_visibility\_level)

Description: Set to public to create a public project. Valid values are "private", "internal" or "public".

Type: `string`

Default: `null`

### <a name="input_wiki_access_level"></a> [wiki\_access\_level](#input\_wiki\_access\_level)

Description: Set the wiki access level. Valid values are "disabled", "private" or "enabled".

Type: `string`

Default: `null`

### <a name="input_wiki_enabled"></a> [wiki\_enabled](#input\_wiki\_enabled)

Description: Enable wiki for the project.

Type: `bool`

Default: `null`

## Outputs

The following outputs are exported:

### <a name="output_access_tokens"></a> [access\_tokens](#output\_access\_tokens)

Description: Project access tokens.

### <a name="output_id"></a> [id](#output\_id)

Description: The ID of project.
<!-- END_TF_DOCS -->
