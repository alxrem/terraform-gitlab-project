# Copyright 2023 Alexey Remizov <alexey@remizov.org>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

variable "name" {
  description = "The name of the project."

  type = string
}

variable "namespace_id" {
  description = "The namespace (group or user) of the project. Defaults to your user."

  type = string

  default = null
}

variable "protected_branches" {
  description = <<-EOF
  The protections of project branches.

  - push_access_level (optional) &mdash; Access levels allowed to push. Valid values are: `no one`, `developer`, `maintainer`.
  - merge_access_level (optional) &mdash; Access levels allowed to push and merge. Valid values are: `no one`, `developer`, `maintainer`.
  - allow_force_push (optional) &mdash; Can be set to `true` to allow users with push access to force push.
  EOF

  type = map(object({
    push_access_level            = optional(string)
    merge_access_level           = optional(string)
    allow_force_push             = optional(bool)
    code_owner_approval_required = optional(bool)
  }))

  default = {}

  validation {
    condition = alltrue([
      for attrs in values(var.protected_branches)
      : contains(
        ["no one", "developer", "maintainer"],
        coalesce(attrs.push_access_level, "no one")
      )
    ])
    error_message = "push_access_level needs to be one of \"no one\", \"developer\" or \"maintainer\""
  }

  validation {
    condition = alltrue([
      for attrs in values(var.protected_branches)
      : contains(
        ["no one", "developer", "maintainer"],
        coalesce(attrs.merge_access_level, "no one")
      )
    ])
    error_message = "merge_access_level needs to be one of \"no one\", \"developer\" or \"maintainer\""
  }
}

variable "variables" {
  description = <<-EOF
  The CI/CD variables of project.

  Names of variables are the keys of map.

  - value &mdash; The value of the variable.
  - description &mdash; The description of the variable.
  - environment_scope &mdash; The environment scope of the variable. Defaults to all environment (`*`). Note that in Community Editions of Gitlab, values other than `*` will cause inconsistent plans.
  - masked &mdash; If set to `true`, the value of the variable will be hidden in job logs. The value must meet the masking requirements.
  - protected &mdash; If set to `true`, the variable will be passed only to pipelines running on protected branches and tags. Defaults to `false`.
  - raw &mdash; Whether the variable is treated as a raw string. Default: `false`. When `true`, variables in the value are not expanded.
  - variable_type &mdash; The type of a variable. Valid values are: `env_var`, `file`. Default is `env_var`.
  EOF

  type = map(object({
    value             = string
    description       = optional(string)
    environment_scope = optional(string)
    masked            = optional(bool)
    protected         = optional(bool)
    raw               = optional(bool)
    variable_type     = optional(string)
  }))

  default = {}

  validation {
    condition = alltrue([
      for attrs in values(var.variables) :
      (attrs.masked != true) || can(regex("^[a-zA-Z0-9_+=/@:.~-]{8,}$", attrs.value))
    ])
    error_message = "Value of masked variable needs to be at least 8 characters long and contains only letters, digits and _+=/@:.~- characters"
  }
}

variable "description" {
  description = "Description of project."

  type = string

  default = null
}

variable "share_groups" {
  description = <<-EOF
  Groups to share project with.

  It is a map the group ids to required access level.

  Example:

  share_groups = {
    "1197" = "maintainer"
    "217"  = "reporter"
  }
  EOF

  type = map(string)

  default = {}

  validation {
    condition = alltrue([
      for level in values(var.share_groups)
      : contains(["owner", "maintainer", "developer", "reporter"], level)
    ])
    error_message = "Access level needs to be one of \"owner\", \"maintainer\", \"developer\" or \"reporter\""
  }
}

variable "share_groups_by_paths" {
  description = <<-EOF
  Groups to share project with.

  It is a map the full group path to required access level.

  Example:

  share_groups = {
    "teams/ops" = "maintainer"
    "teams/qa"  = "reporter"
  }
  EOF

  type = map(string)

  default = {}

  validation {
    condition = alltrue([
      for level in values(var.share_groups_by_paths)
      : contains(["owner", "maintainer", "developer", "reporter"], level)
    ])
    error_message = "Access level needs to be one of \"owner\", \"maintainer\", \"developer\" or \"reporter\""
  }
}

variable "access_tokens" {
  description = <<-DESCRIPTION
  Project access tokens.

  Key of map is the name to describe the project access token.

  Values describe the attributes of the access tokens.

  - scopes &mdash; The scope for the project access token. It determines
    the actions which can be performed when authenticating with this token.
    Valid values are: `api`, `read_api`, `read_registry`, `write_registry`,
    `read_repository`, `write_repository`, `create_runner`.
  - expires (optional) &mdash; Time the token will expire it, YYYY-MM-DD format;
    by default one year from date of creation.
  - access_level (optional) &mdash; The access level for the project access token.
    Valid values are: `no one`, `minimal`, `guest`, `reporter`, `developer`,
    `maintainer`, `owner`, `master`. Default is `maintainer`.
  DESCRIPTION

  type = map(object({
    scopes       = set(string)
    expires_at   = optional(string)
    access_level = optional(string)
  }))

  default = {}

  validation {
    condition = alltrue([
      for token in values(var.access_tokens)
      : length(setsubtract(token.scopes, ["api", "read_api", "read_registry", "write_registry", "read_repository", "write_repository", "create_runner"])) == 0
    ])
    error_message = "access_tokens.scopes needs to be one of \"api\", \"read_api\", \"read_registry\", \"write_registry\", \"read_repository\", \"write_repository\" or \"create_runner\""
  }

  validation {
    condition = alltrue([
      for token in values(var.access_tokens)
      : contains(
        ["no one", "minimal", "guest", "reporter", "developer", "maintainer", "owner", "master"],
        coalesce(token.access_level, "maintainer")
      )
    ])
    error_message = "access_tokens.access_level needs to be one of \"no one\", \"minimal\", \"guest\", \"reporter\", \"developer\", \"maintainer\", \"owner\" or \"master\""
  }

  validation {
    condition = alltrue([
      for token in values(var.access_tokens)
      : can(formatdate("YYYY-MM-DD", "${coalesce(token.expires_at, "2024-03-31")}T00:00:00Z"))
    ])
    error_message = "access_tokens.expires_at needs to be correct date in format YYYY-MM-DD or null."
  }
}

variable "allow_merge_on_skipped_pipeline" {
  description = "Set to true if you want to treat skipped pipelines as if they finished with success."

  type = bool

  default = null
}

variable "analytics_access_level" {
  description = "Set the analytics access level. Valid values are disabled, private, enabled."

  type = string

  default = null

  validation {
    condition = contains([
      "disabled", "private", "enabled"
    ], coalesce(var.analytics_access_level, "disabled"))
    error_message = "analytics_access_level needs to be one of \"disabled\", \"private\" or \"enabled\""
  }
}

variable "approvals_before_merge" {
  description = "Number of merge request approvals required for merging (EE only)."

  type = number

  default = null
}

variable "archive_on_destroy" {
  description = "Set to true to archive the project instead of deleting on destroy."

  type = bool

  default = null
}

variable "archived" {
  description = "Whether the project is in read-only mode (archived). Repositories can be archived/unarchived by toggling this parameter"

  type = bool

  default = null
}

variable "auto_cancel_pending_pipelines" {
  description = "Auto-cancel pending pipelines."

  type = bool

  default = null
}

variable "auto_devops_deploy_strategy" {
  description = "Auto Deploy strategy. Valid values are continuous, manual, timed_incremental."

  type = string

  default = null

  validation {
    condition = contains(
      ["continuous", "manual", "timed_incremental"],
      coalesce(var.auto_devops_deploy_strategy, "continuous")
    )
    error_message = "auto_devops_deploy_strategy needs to be one of \"continuous\", \"manual\" or \"timed_incremental\""
  }
}

variable "auto_devops_enabled" {
  description = "Enable Auto DevOps for this project."

  type = bool

  default = null
}

variable "autoclose_referenced_issues" {
  description = "Set whether auto-closing referenced issues on default branch."

  type = bool

  default = null
}

variable "avatar" {
  description = "A local path to the avatar image to upload."

  type = string

  default = null
}

variable "avatar_hash" {
  description = <<EOF
  The hash of the avatar image. this is used to trigger an update of the avatar.
  If it's not given, but an avatar is given, the avatar will be updated each time.
  EOF

  type = string

  default = null
}

variable "build_git_strategy" {
  description = " The Git strategy. Defaults to fetch. Valid values are \"clone\", \"fetch\"."

  type = string

  default = null

  validation {
    condition = contains(
      ["clone", "fetch"],
      coalesce(var.build_git_strategy, "fetch")
    )
    error_message = "build_git_strategy needs to be one of \"clone\", or \"fetch\""
  }
}

variable "build_timeout" {
  description = "The maximum amount of time, in seconds, that a job can run."

  type = number

  default = null

  validation {
    condition     = 600 <= coalesce(var.build_timeout, 600) && coalesce(var.build_timeout, 600) <= 2629746
    error_message = "build_timeout needs to be between 600 and 2629746"
  }
}

variable "builds_access_level" {
  description = "Set the builds access level. Valid values are \"disabled\", \"private\", \"enabled\"."

  type = string

  default = null

  validation {
    condition = contains(
      ["disabled", "private", "enabled"],
      coalesce(var.builds_access_level, "disabled")
    )
    error_message = "builds_access_level needs to be one of \"disabled\", \"private\" or \"enabled\""
  }
}

variable "ci_config_path" {
  description = "Custom Path to CI config file."

  type = string

  default = null
}

variable "ci_default_git_depth" {
  description = "Default number of revisions for shallow cloning."

  type = number

  default = null
}

variable "ci_forward_deployment_enabled" {
  description = "When a new deployment job starts, skip older deployment jobs that are still pending."

  type = bool

  default = null
}

variable "ci_separated_caches" {
  description = "Use separate caches for protected branches."

  type = bool

  default = null
}

variable "container_expiration_policy" {
  description = <<-EOT
  Set the image cleanup policy for this project.
  EOT

  type = object({
    cadence           = optional(string)
    enabled           = optional(bool, true)
    keep_n            = optional(number)
    name_regex        = optional(string)
    name_regex_delete = optional(string)
    name_regex_keep   = optional(string)
    older_than        = optional(string)
  })

  default = null

  validation {
    condition = contains(
      ["1d", "7d", "14d", "1month", "3month"],
      coalesce(try(var.container_expiration_policy.cadence, null), "1d")
    )
    error_message = "container_expiration_policy.cadence needs to be one of \"1d\", \"7d\", \"14d\", \"1month\" or \"3month\""
  }

  validation {
    condition = contains(
      [1, 5, 10, 25, 50, 100],
      coalesce(try(var.container_expiration_policy.keep_n, null), 1)
    )
    error_message = "container_expiration_policy.keep_n needs to be one of 1, 5, 10, 25, 50 or 100"
  }

  validation {
    condition = contains(
      ["7d", "14d", "30d", "90d"],
      coalesce(try(var.container_expiration_policy.older_than, null), "7d")
    )
    error_message = "container_expiration_policy.older_than needs to be one of \"7d\", \"14d\", \"30d\" or \"90d\""
  }

  validation {
    condition = (
      var.container_expiration_policy == null
      || !(
        try(var.container_expiration_policy.name_regex, null) != null
        && try(var.container_expiration_policy.name_regex_keep, null) != null
      )
    )
    error_message = "container_expiration_policy.name_regex can't be used together with container_expiration_policy.name_regex_keep"
  }

  validation {
    condition = (
      var.container_expiration_policy == null
      || !(
        try(var.container_expiration_policy.name_regex, null) != null
        && try(var.container_expiration_policy.name_regex_delete, null) != null
      )
    )
    error_message = "container_expiration_policy.name_regex can't be used together with container_expiration_policy.name_regex_delete"
  }
}

variable "container_registry_access_level" {
  description = "Set visibility of container registry, for this project. Valid values are \"disabled\", \"private\" or \"enabled\"."

  type = string

  default = null

  validation {
    condition = contains(
      ["disabled", "private", "enabled"],
      coalesce(var.container_registry_access_level, "disabled")
    )
    error_message = "container_registry_access_level needs to be one of \"disabled\", \"private\" or \"enabled\""
  }
}

variable "container_registry_enabled" {
  description = "Enable container registry for the project."

  type = bool

  default = null
}

variable "default_branch" {
  description = "The default branch for the project."

  type = string

  default = null
}

variable "emails_enabled" {
  description = "Enable email notifications."

  type = bool

  default = null
}

variable "environments_access_level" {
  description = "Set the environments access level. Valid values are \"disabled\", \"private\" or \"enabled\"."

  type = string

  default = null

  validation {
    condition = contains(
      ["disabled", "private", "enabled"],
      coalesce(var.environments_access_level, "disabled")
    )
    error_message = "environment_access_level needs to be one of \"disabled\", \"private\" or \"enabled\""
  }
}

variable "external_authorization_classification_label" {
  description = "The classification label for the project."

  type = string

  default = null
}

variable "feature_flags_access_level" {
  description = "Set the feature flags level. Valid values are \"disabled\", \"private\" or \"enabled\"."

  type = string

  default = null

  validation {
    condition = contains(
      ["disabled", "private", "enabled"],
      coalesce(var.feature_flags_access_level, "disabled")
    )
    error_message = "feature_flags_access_level needs to be one of \"disabled\", \"private\" or \"enabled\""
  }
}

variable "forked_from_project_id" {
  description = <<EOT
  The id of the project to fork. During create the project is forked
  and during an update the fork relation is changed.
  EOT

  type = number

  default = null
}

variable "forking_access_level" {
  description = "Set the feature flags level. Valid values are \"disabled\", \"private\" or \"enabled\"."

  type = string

  default = null

  validation {
    condition = contains(
      ["disabled", "private", "enabled"],
      coalesce(var.forking_access_level, "disabled")
    )
    error_message = "forking_access_level needs to be one of \"disabled\", \"private\" or \"enabled\""
  }
}

variable "group_with_project_templates_id" {
  description = <<EOT
  For group-level custom templates, specifies ID of group from which
  all the custom project templates are sourced. Leave empty
  for instance-level templates. Requires use_custom_template to be true
  (enterprise edition).
  EOT

  type = number

  default = null
}

variable "import_url" {
  description = <<EOT
  Git URL to a repository to be imported. Together with `mirror = true`
  it will setup a Pull Mirror. This can also be used together
  with `forked_from_project_id` to setup a Pull Mirror for a fork.
  The fork takes precedence over the import. Make sure to provide
  the credentials in `import_url_username` and `import_url_password`.
  GitLab never returns the credentials, thus the provider cannot detect
  configuration drift in the credentials. They can also not be imported
  using terraform import. See the examples section for how to properly use it.
  EOT

  type = string

  default = null
}

variable "import_url_password" {
  description = <<EOT
  The password for the `import_url`. The value of this field is used
  to construct a valid `import_url` and is only related to the provider.
  This field cannot be imported using terraform import.
  EOT

  type = string

  sensitive = true

  default = null
}

variable "import_url_username" {
  description = <<EOF
  The username for the `import_url`. The value of this field is used
  to construct a valid `import_url` and is only related to the provider.
  This field cannot be imported using terraform import.
  EOF

  type = string

  default = null
}

variable "infrastructure_access_level" {
  description = "Set the infrastructure access level. Valid values are \"disabled\", \"private\" or \"enabled\"."

  type = string

  default = null

  validation {
    condition = contains(
      ["disabled", "private", "enabled"],
      coalesce(var.infrastructure_access_level, "disabled")
    )
    error_message = "infrastructure_access_level needs to be one of \"disabled\", \"private\" or \"enabled\""
  }
}

variable "initialize_with_readme" {
  description = "Create main branch with first commit containing a README.md file."

  type = string

  default = null
}

variable "issues_access_level" {
  description = "Set the issues access level. Valid values are \"disabled\", \"private\" or \"enabled\"."

  type = string

  default = null

  validation {
    condition = contains(
      ["disabled", "private", "enabled"],
      coalesce(var.issues_access_level, "disabled")
    )
    error_message = "issues_access_level needs to be one of \"disabled\", \"private\" or \"enabled\""
  }
}

variable "issues_enabled" {
  description = "Enable issue tracking for the project."

  type = bool

  default = null
}

variable "issues_template" {
  description = "Sets the template for new issues in the project."

  type = string

  default = null
}

variable "keep_latest_artifact" {
  description = "Disable or enable the ability to keep the latest artifact for this project."

  type = bool

  default = null
}

variable "lfs_enabled" {
  description = "Enable LFS for the project."

  type = bool

  default = null
}

variable "merge_commit_template" {
  description = "Template used to create merge commit message in merge requests."

  type = string

  default = null
}

variable "merge_method" {
  description = "Set the merge method. Valid values are \"merge\", \"rebase_merge\" or \"ff\"."

  type = string

  default = null

  validation {
    condition = contains(
      ["merge", "rebase_merge", "ff"],
      coalesce(var.merge_method, "merge")
    )
    error_message = "merge_method needs to be one of \"merge\", \"rebase_merge\" or \"ff\""
  }
}

variable "merge_requests_enabled" {
  description = "Enable merge requests for the project."

  type = bool

  default = null
}

variable "merge_requests_template" {
  description = "Sets the template for new merge requests in the project."

  type = string

  default = null
}

variable "merge_trains_enabled" {
  description = "Enable or disable merge trains. Requires `merge_pipelines_enabled` to be set to true to take effect."

  type = bool

  default = null
}

variable "mirror" {
  description = "Enable project pull mirror."

  type = bool

  default = null
}

variable "mirror_overwrites_diverged_branches" {
  description = "Enable overwrite diverged branches for a mirrored project."

  type = bool

  default = null
}

variable "mirror_trigger_builds" {
  description = "Enable trigger builds on pushes for a mirrored project."

  type = bool

  default = null
}

variable "monitor_access_level" {
  description = "Set the monitor access level. Valid values are \"disabled\", \"private\" or \"enabled\"."

  type = string

  default = null

  validation {
    condition = contains(
      ["disabled", "private", "enabled"],
      coalesce(var.monitor_access_level, "disabled")
    )
    error_message = "monitor_access_level needs to be one of \"disabled\", \"private\" or \"enabled\""
  }
}

variable "mr_default_target_self" {
  description = "For forked projects, target merge requests to this project. If false, the target will be the upstream project."

  type = bool

  default = null
}

variable "only_allow_merge_if_all_discussions_are_resolved" {
  description = "Set to true if you want allow merges only if all discussions are resolved."

  type = bool

  default = null
}

variable "only_allow_merge_if_pipeline_succeeds" {
  description = "Set to true if you want allow merges only if a pipeline succeeds."

  type = bool

  default = null
}

variable "only_mirror_protected_branches" {
  description = "Enable only mirror protected branches for a mirrored project."

  type = bool

  default = null
}

variable "packages_enabled" {
  description = "Enable packages repository for the project."

  type = bool

  default = null
}

variable "pages_access_level" {
  description = "Enable pages access control. Valid values are \"public\", \"private\", \"enabled\" or \"disabled\"."

  type = string

  default = null


  validation {
    condition = contains(
      ["public", "private", "enabled", "disabled"],
      coalesce(var.pages_access_level, "disabled")
    )
    error_message = "pages_access_level needs to be one of \"disabled\", \"private\", \"public\" or \"enabled\"."
  }
}

variable "path" {
  description = "The path of the repository."

  type = string

  default = null
}

variable "pipelines_enabled" {
  description = "Enable pipelines for the project."

  type = bool

  default = null
}

variable "printing_merge_request_link_enabled" {
  description = "Show link to create/view merge request when pushing from the command line."

  type = bool

  default = null
}

variable "public_builds" {
  description = "If true, jobs can be viewed by non-project members."

  type = bool

  default = null
}

variable "public_jobs" {
  description = " If true, jobs can be viewed by non-project members."

  type = bool

  default = null
}

variable "push_rules" {
  description = "Push rules for the project"

  type = object({
    author_email_regex            = optional(string)
    branch_name_regex             = optional(string)
    commit_committer_check        = optional(bool)
    commit_message_negative_regex = optional(string)
    commit_message_regex          = optional(string)
    deny_delete_tag               = optional(bool)
    file_name_regex               = optional(string)
    max_file_size                 = optional(number)
    member_check                  = optional(bool)
    prevent_secrets               = optional(bool)
    reject_unsigned_commits       = optional(bool)
  })

  default = null
}

variable "releases_access_level" {
  description = "Set the releases access level. Valid values are \"disabled\", \"private\" or \"enabled\"."

  type = string

  default = null

  validation {
    condition = contains(
      ["disabled", "private", "enabled"],
      coalesce(var.releases_access_level, "disabled")
    )
    error_message = "releases_access_level needs to be one of \"disabled\", \"private\" or \"enabled\""
  }
}

variable "remove_source_branch_after_merge" {
  description = "Enable Delete source branch option by default for all new merge requests."

  type = bool

  default = null
}

variable "repository_access_level" {
  description = "Set the repository access level. Valid values are \"disabled\", \"private\" or \"enabled\"."

  type = string

  default = null

  validation {
    condition = contains(
      ["disabled", "private", "enabled"],
      coalesce(var.repository_access_level, "disabled")
    )
    error_message = "repository_access_level needs to be one of \"disabled\", \"private\" or \"enabled\""
  }
}

variable "repository_storage" {
  description = "Which storage shard the repository is on. (administrator only)"

  type = string

  default = null
}

variable "request_access_enabled" {
  description = "Allow users to request member access."

  type = string

  default = null
}

variable "requirements_access_level" {
  description = "Set the requirements access level. Valid values are \"disabled\", \"private\" or \"enabled\"."

  type = string

  default = null

  validation {
    condition = contains(
      ["disabled", "private", "enabled"],
      coalesce(var.requirements_access_level, "disabled")
    )
    error_message = "requirements_access_level needs to be one of \"disabled\", \"private\" or \"enabled\""
  }
}

variable "resolve_outdated_diff_discussions" {
  description = "Automatically resolve merge request diffs discussions on lines changed with a push."

  type = bool

  default = null
}

variable "restrict_user_defined_variables" {
  description = "Allow only users with the Maintainer role to pass user-defined variables when triggering a pipeline."

  type = bool

  default = null
}

variable "security_and_compliance_access_level" {
  description = "Set the security_and_compliance access level. Valid values are \"disabled\", \"private\" or \"enabled\"."

  type = string

  default = null

  validation {
    condition = contains(
      ["disabled", "private", "enabled"],
      coalesce(var.security_and_compliance_access_level, "disabled")
    )
    error_message = "security_and_compliance_access_level needs to be one of \"disabled\", \"private\" or \"enabled\""
  }
}

variable "shared_runners_enabled" {
  description = "Enable shared runners for this project."

  type = bool

  default = null
}

variable "skip_wait_for_default_branch_protection" {
  description = <<EOT
  If true, the default behavior to wait for the default branch protection
  to be created is skipped. This is necessary if the current user
  is not an admin and the default branch protection is disabled on
  an instance-level. There is currently no known way to determine
  if the default branch protection is disabled on an instance-level
  for non-admin users. This attribute is only used during resource creation,
  thus changes are suppressed and the attribute cannot be imported.
  EOT

  type = bool

  default = null
}

variable "snippets_access_level" {
  description = "Set the snippets access level. Valid values are \"disabled\", \"private\" or \"enabled\"."

  type = string

  default = null

  validation {
    condition = contains(
      ["disabled", "private", "enabled"],
      coalesce(var.snippets_access_level, "disabled")
    )
    error_message = "snippets_access_level needs to be one of \"disabled\", \"private\" or \"enabled\""
  }
}

variable "snippets_enabled" {
  description = "Enable snippets for the project."

  type = bool

  default = null
}

variable "squash_commit_template" {
  description = "Template used to create squash commit message in merge requests."

  type = string

  default = null
}

variable "squash_option" {
  description = "Squash commits when merge request. Valid values are \"never\", \"always\", \"default_on\" or \"default_off\". The default value is \"default_off\"."

  type = string

  default = null

  validation {
    condition = contains(
      ["never", "always", "default_on", "default_off"],
      coalesce(var.squash_option, "never")
    )
    error_message = "squash_option needs to be one of \"never\", \"always\", \"default_on\" or \"default_off\""
  }
}

variable "suggestion_commit_message" {
  description = "The commit message used to apply merge request suggestions."

  type = string

  default = null
}

variable "tags" {
  description = "The list of tags for a project; put array of tags, that should be finally assigned to a project. Use topics instead."

  type = set(string)

  default = null
}

variable "template_name" {
  description = "When used without use_custom_template, name of a built-in project template. When used with use_custom_template, name of a custom project template. This option is mutually exclusive with template_project_id."

  type = string

  default = null
}

variable "template_project_id" {
  description = <<EOT
  When used with use_custom_template, project ID of a custom project template.
  This is preferable to using template_name since template_name
  may be ambiguous (enterprise edition). This option is mutually exclusive
  with template_name. See gitlab_group_project_file_template to set a project
  as a template project. If a project has not been set as a template,
  using it here will result in an error.
  EOT

  type = number

  default = null
}

variable "topics" {
  description = "The list of topics for the project."

  type = set(string)

  default = null
}

variable "use_custom_template" {
  description = <<EOT
  Use either custom instance or group (with group_with_project_templates_id)
  project template (enterprise edition). ~> When using a custom template,
  Group Tokens won't work. You must use a real user's Personal Access Token.
  EOT

  type = bool

  default = null
}

variable "visibility_level" {
  description = "Set to public to create a public project. Valid values are \"private\", \"internal\" or \"public\"."

  type = string

  default = null

  validation {
    condition = contains(
      ["private", "internal", "public"],
      coalesce(var.visibility_level, "private")
    )
    error_message = "visibility_level needs to be one of \"private\", \"internal\" or \"public\""
  }
}

variable "wiki_access_level" {
  description = "Set the wiki access level. Valid values are \"disabled\", \"private\" or \"enabled\"."

  type = string

  default = null

  validation {
    condition = contains(
      ["disabled", "private", "enabled"],
      coalesce(var.wiki_access_level, "disabled")
    )
    error_message = "wiki_access_level needs to be one of \"disabled\", \"private\" or \"enabled\""
  }
}

variable "wiki_enabled" {
  description = "Enable wiki for the project."

  type = bool

  default = null
}

variable "integration_pipelines_email" {
  description = <<-EOF
  The integration with Pipeline Emails Service.

  - recipients &mdash; email addresses where notifications are sent.
  - branches_to_be_notified (optional) &mdash; Branches to send notifications for. Valid options are all, default, protected, and default_and_protected. Default is default
  - notify_only_broken_pipelines (optional) &mdash; Notify only broken pipelines. Default is true.
  EOF

  type = object({
    recipients                   = set(string)
    notify_only_broken_pipelines = optional(bool)
    branches_to_be_notified      = optional(string)
  })

  default = null

  validation {
    condition = contains(
      ["all", "default", "protected", "default_and_protected"],
      try(var.integration_pipelines_email.branches_to_be_notified, "default")
    )
    error_message = "branches_to_be_notified needs to be one of all, default, protected or default_and_protected"
  }
}

variable "integration_telegram" {
  description = <<DESCRIPTION
  The integration with Telegram service.

  - token &mdash; The Telegram bot token.
  - room &mdash; Unique identifier for the target chat or the username of the target channel (in the format @channelusername).

  - notify_only_broken_pipelines &mdash; Send notifications for broken pipelines.
  - branches_to_be_notified &mdash; Branches to send notifications for (introduced in GitLab 16.5). Update of this attribute was not supported before Gitlab 16.11 due to API bug. Valid options are all, default, protected, default_and_protected.
  - push_events &mdash; Enable notifications for push events.
  - issues_events &mdash; Enable notifications for issues events.
  - confidential_issues_events &mdash; Enable notifications for confidential issues events.
  - merge_requests_events &mdash; Enable notifications for merge requests events.
  - tag_push_events &mdash; Enable notifications for tag push events.
  - note_events &mdash; Enable notifications for note events.
  - confidential_note_events &mdash; Enable notifications for confidential note events.
  - pipeline_events &mdash; Enable notifications for pipeline events.
  - wiki_page_events &mdash; Enable notifications for wiki page events.
  DESCRIPTION

  type = object({
    token = string
    room  = string

    notify_only_broken_pipelines = optional(bool, true)
    branches_to_be_notified      = optional(string, "default")
    push_events                  = optional(bool, false)
    issues_events                = optional(bool, false)
    confidential_issues_events   = optional(bool, false)
    merge_requests_events        = optional(bool, false)
    tag_push_events              = optional(bool, false)
    note_events                  = optional(bool, false)
    confidential_note_events     = optional(bool, false)
    pipeline_events              = optional(bool, false)
    wiki_page_events             = optional(bool, false)
  })

  default = null

  validation {
    condition = contains(
      ["all", "default", "protected", "default_and_protected"],
      try(var.integration_telegram.branches_to_be_notified, "default")
    )
    error_message = "branches_to_be_notified needs to be one of all, default, protected or default_and_protected"
  }
}
