# Copyright 2023 Alexey Remizov <alexey@remizov.org>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import pytest
import tftest


@pytest.fixture
def groups_to_share(preinstalled):
    config = {
        "variable": {
            "groups": {
                "type": "set(string)",
                "default": ["shared1", "shared2"]
            }
        },
        "resource": {
            "gitlab_group": {
                "default": {
                    "for_each": "${var.groups}",
                    "name": "${each.value}",
                    "path": "${each.value}",
                }
            },
        },
        "output": {
            "groups": {
                "value": "${ {for name, group in gitlab_group.default"
                         " : name => group.id} }",
                "sensitive": True,
            }
        }
    }
    with preinstalled(config) as output:
        yield output["groups"]


@pytest.fixture
def variables(uuid, groups_to_share):
    return dict(
        name=f"terraform-test-project-{uuid}",
        protected_branches={
            "main": {
                "push_access_level": "no one",
                "merge_access_level": "developer",
                "allow_force_push": True,
            }
        },
        variables={
            "TEST_CI_VAR": {
                "value": "test+value",
                "masked": True,
                "variable_type": "file",
            }
        },
        integration_pipelines_email={
            "recipients": ["admin@example.org"],
            "notify_only_broken_pipelines": False,
            "branches_to_be_notified": "protected",
        },
        description="test project description",
        share_groups={
            groups_to_share["shared1"]: "reporter",
        },
        share_groups_by_paths={
            "shared2": "reporter",
        },
        access_tokens={
            "test_token": {
                "scopes": ["api"],
            }
        },
        allow_merge_on_skipped_pipeline=True,
        analytics_access_level="enabled",
        approvals_before_merge=0,
        archive_on_destroy=True,
        archived=False,
        auto_cancel_pending_pipelines=True,
        auto_devops_deploy_strategy="manual",
        auto_devops_enabled=True,
        autoclose_referenced_issues=True,
        build_git_strategy="clone",
        build_timeout=600,
        builds_access_level="private",
        ci_config_path="gitlab-ci.yaml",
        ci_default_git_depth=10,
        ci_forward_deployment_enabled=True,
        ci_separated_caches=True,
        container_expiration_policy={
            "cadence": "1d",
            "keep_n": 5,
            # Conflicts with name_regex_*, needs special case
            # "name_regex": "image.*",
            "name_regex_delete": "image.*",
            "name_regex_keep": "image.*",
            "older_than": "7d",
        },
        container_registry_access_level="enabled",
        container_registry_enabled=True,
        # Always "main" on CE (?)
        # "default_branch": "master",
        emails_enabled=True,
        environments_access_level="private",
        # Always "" on CE
        # "external_authorization_classification_label": "label",
        feature_flags_access_level="private",
        # Needs special case with preinstalled project
        # "forked_from_project_id": 1,
        forking_access_level="private",
        infrastructure_access_level="private",
        initialize_with_readme=False,
        issues_access_level="private",
        issues_enabled=True,
        issues_template="",
        keep_latest_artifact=True,
        lfs_enabled=False,
        merge_commit_template="tpl",
        merge_method="ff",
        merge_requests_enabled=False,
        merge_requests_template="",
        # Needs special case with import_url
        # "mirror": True,
        # "mirror_overwrites_diverged_branches": True,
        # "mirror_trigger_builds": True,
        monitor_access_level="private",
        # Needs special case with forked project
        # "mr_default_target_self": False,
        # Needs special case with preinstalled group
        # "namespace_id": 1,
        only_allow_merge_if_all_discussions_are_resolved=True,
        only_allow_merge_if_pipeline_succeeds=True,
        # Needs special case with import_url
        # only_mirror_protected_branches=False,
        packages_enabled=True,
        pages_access_level="public",
        path=f"custom-path-{uuid}",
        pipelines_enabled=True,
        printing_merge_request_link_enabled=True,
        # public_builds=True,
        public_jobs=True,
        # Needs EE for testing
        # push_rules = {}
        releases_access_level="private",
        remove_source_branch_after_merge=True,
        repository_access_level="private",
        # repository_storage="storage",
        request_access_enabled=True,
        # Always "" on CE (?)
        # requirements_access_level="",
        resolve_outdated_diff_discussions=True,
        restrict_user_defined_variables=True,
        security_and_compliance_access_level="private",
        shared_runners_enabled=True,
        skip_wait_for_default_branch_protection=True,
        snippets_access_level="private",
        snippets_enabled=True,
        squash_commit_template="template",
        squash_option="always",
        suggestion_commit_message="message",
        tags=["a", "b"],
        # Needs EE for testing
        # template_name = ""
        # template_project_id = ""
        # use_custom_template = True
        visibility_level="private",
        wiki_access_level="private",
        wiki_enabled=True,
    )


def assert_resource_changed(
        output: tftest.TerraformPlanOutput, resource: str, change: str):
    if resource not in output.resource_changes:
        pytest.fail(f'No "{resource}" in changed')
    actions = output.resource_changes[resource]["change"]["actions"]
    if change not in actions:
        pytest.fail(f'No "{change}" action in {", ".join(actions)}')


def test_apply(gitlab, tf, module_version, groups_to_share):
    with module_version("HEAD"):
        tf.init()
        try:
            output = tf.plan(output=True)
            for resource in [
                'gitlab_branch_protection.default["main"]',
                'gitlab_integration_pipelines_email.default[0]',
                'gitlab_project.default',
                f'gitlab_project_share_group.default["{groups_to_share["shared1"]}"]',
                f'gitlab_project_share_group.default["{groups_to_share["shared2"]}"]',
                'gitlab_project_variable.default["TEST_CI_VAR"]',
                'gitlab_project_access_token.default["test_token"]',
            ]:
                assert_resource_changed(output, resource, "create")
            tf.apply()
            print(tf.plan())
            assert ("No changes. Your infrastructure "
                    "matches the configuration.") in tf.plan()
        finally:
            tf.destroy()
