# Copyright 2023 Alexey Remizov <alexey@remizov.org>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import pytest

from conftest import skip_without_gitlab


class Case(dict):
    error_message = None

    def with_error(self, message):
        self.error_message = message
        return self


CASES = {
    "protected_branches.push_access_level": Case({
        "protected_branches": {
            "main": {"push_access_level": "unknown", }
        }
    }).with_error('push_access_level needs to be one of '
                  '"no one", "developer" or "maintainer"'),

    "protected_branches.merge_access_level": Case({
        "protected_branches": {
            "main": {"merge_access_level": "unknown", }
        }
    }).with_error('merge_access_level needs to be one of '
                  '"no one", "developer" or "maintainer"'),

    "masked variables.value": Case({
        "variables": {
            "VAR": {"masked": True, "value": "invalid value"}
        }
    }).with_error("Value of masked variable needs to be"),

    "required variables.value": Case({
        "variables": {
            "VAR": {}
        }
    }).with_error('attribute "value" is required.'),

    "share_groups.access_level": Case({
        "share_groups": {
            "group": "unknown",
        }
    }).with_error('Access level needs to be one of'),

    "analytics_access_level": Case({
        "analytics_access_level": "unknown"
    }).with_error('analytics_access_level needs to be one of'),

    "build_git_strategy": Case({
        "build_git_strategy": "unknown"
    }).with_error('build_git_strategy needs to be one of'),

    "builds_access_level": Case({
        "builds_access_level": "unknown"
    }).with_error('builds_access_level needs to be one of'),

    "container_expiration_policy.cadence": Case({
        "container_expiration_policy": {"cadence": "5d"}
    }).with_error('container_expiration_policy.cadence needs to be one of'),

    "container_expiration_policy.keep_n": Case({
        "container_expiration_policy": {
            "keep_n": 3,
        }
    }).with_error('container_expiration_policy.keep_n needs to be one of'),

    "container_expiration_policy.older_than": Case({
        "container_expiration_policy": {
            "older_than": "1d",
        }
    }).with_error('container_expiration_policy.older_than needs to be one of'),

    "container_expiration_policy.name_regex conflict with name_regex_keep":
        Case({
            "container_expiration_policy": {
                "name_regex": "a",
                "name_regex_keep": "b",
            }
        }).with_error(r"container_expiration_policy.name_regex can.*together"),

    "container_expiration_policy.name_regex conflict with name_regex_delete":
        Case({
            "container_expiration_policy": {
                "name_regex": "a",
                "name_regex_delete": "b",
            }
        }).with_error(r"container_expiration_policy.name_regex can.*together"),

    "container_registry_access_level": Case({
        "container_registry_access_level": "unknown"
    }).with_error('container_registry_access_level needs to be one of'),

    "enabled container registry conflicts with disabled registry access level":
        Case({
            "container_registry_enabled": True,
            "container_registry_access_level": "disabled",
        }).with_error("Inconsistent values of var.container_registry_enabled"),

    "disabled container registry conflicts with enabled registry access level":
        Case({
            "container_registry_enabled": False,
            "container_registry_access_level": "enabled",
        }).with_error("Inconsistent values of var.container_registry_enabled"),

    "environments_access_level": Case({
        "environments_access_level": "unknown"
    }).with_error('environment_access_level needs to be one of'),

    "feature_flags_access_level": Case({
        "feature_flags_access_level": "unknown"
    }).with_error('feature_flags_access_level needs to be one of'),

    "forking_access_level": Case({
        "forking_access_level": "unknown"
    }).with_error('forking_access_level needs to be one of'),

    "infrastructure_access_level": Case({
        "infrastructure_access_level": "unknown"
    }).with_error('infrastructure_access_level needs to be one of'),

    "issues_access_level": Case({
        "issues_access_level": "unknown"
    }).with_error('issues_access_level needs to be one of'),

    "import conflicts with readme": Case({
        "import_url": "https://example.org",
        "initialize_with_readme": True,
    }).with_error("import_url can not be used with initialize_with_readme"),

    "merge_method": Case({
        "merge_method": "unknown"
    }).with_error('merge_method needs to be one of'),

    "monitor_access_level": Case({
        "monitor_access_level": "unknown"
    }).with_error('monitor_access_level needs to be one of'),

    "pages_access_level": Case({
        "pages_access_level": "unknown"
    }).with_error('pages_access_level needs to be one of'),

    "mr_default_target_self requires forked_from_project_id": Case({
        "mr_default_target_self": False,
    }).with_error("mr_default_target_self "
                  "requires forked_from_project_id to be set"),

    "only_mirror_protected_branches requires import_url": Case({
        "only_mirror_protected_branches": True,
    }).with_error("only_mirror_protected_branches "
                  "requires import_url to be set"),

    "public_jobs conflicts with public_builds": Case({
        "public_jobs": True,
        "public_builds": True,
    }).with_error("public_jobs can not be used with public_builds"),

    "releases_access_level": Case({
        "releases_access_level": "unknown"
    }).with_error('releases_access_level needs to be one of'),

    "repository_access_level": Case({
        "repository_access_level": "unknown"
    }).with_error('repository_access_level needs to be one of'),

    "requirements_access_level": Case({
        "requirements_access_level": "unknown"
    }).with_error('requirements_access_level needs to be one of'),

    "security_and_compliance_access_level": Case({
        "security_and_compliance_access_level": "unknown"
    }).with_error('security_and_compliance_access_level needs to be one of'),

    "snippets_access_level": Case({
        "snippets_access_level": "unknown"
    }).with_error('snippets_access_level needs to be one of'),

    "squash_option": Case({
        "squash_option": "unknown"
    }).with_error('squash_option needs to be one of'),

    "visibility_level": Case({
        "visibility_level": "unknown"
    }).with_error('visibility_level needs to be one of'),

    "wiki_access_level": Case({
        "wiki_access_level": "unknown"
    }).with_error('wiki_access_level needs to be one of'),

    "token.scopes": Case({
        "access_tokens": {
            "test_token": {
                "scopes": ["unknown"],
            }
        }
    }).with_error('access_tokens.scopes needs to be one of'),

    "token.access_level": Case({
        "access_tokens": {
            "test_token": {
                "scopes": ["api"],
                "access_level": "unknown",
            }
        }
    }).with_error('access_tokens.access_level needs to be one of'),

    "token.expires_at": Case({
        "access_tokens": {
            "test_token": {
                "scopes": ["api"],
                "expires_at": "2024-13-11",
            }
        }
    }).with_error('access_tokens.expires_at needs to be'),

    "integration_pipelines_email.recipients": Case({
        "integration_pipelines_email": {}
    }).with_error('attribute "recipients" is required.'),

    "integration_pipelines_email.branches_to_be_notified": Case({
        "integration_pipelines_email": {
            "recipients": ["admin@example.org"],
            "branches_to_be_notified": "unknown",
        }
    }).with_error('branches_to_be_notified needs to be one of'),

    "integration_telegram.token": Case({
        "integration_telegram": {
            "room": -1000000000,
        }
    }).with_error('attribute "token" is required.'),

    "integration_telegram.room": Case({
        "integration_telegram": {
            "token": "token",
        }
    }).with_error('attribute "room" is required.'),
}


@pytest.fixture(params=CASES.keys())
def variables(project_name, request):
    vars_ = CASES[request.param]
    vars_["name"] = project_name
    return vars_


def test_plan(plan_failure, variables: Case, request):
    if any(word in request.node.name for word in ["requires", "conflicts"]):
        skip_without_gitlab()
    assert plan_failure.match(variables.error_message)
