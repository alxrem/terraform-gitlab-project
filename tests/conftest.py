# Copyright 2023 Alexey Remizov <alexey@remizov.org>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json
import os
import shutil
from collections.abc import Mapping
from contextlib import contextmanager
from functools import partial
from pathlib import Path
from subprocess import check_output
from uuid import uuid4

import pytest
import tftest
from semver import Version


@pytest.fixture
def uuid():
    return uuid4().hex


@pytest.fixture
def project_name(uuid):
    return f"project-{uuid}"


def skip_without_gitlab():
    required_env_vars = {"GITLAB_TOKEN", "GITLAB_BASE_URL"}
    if required_env_vars - os.environ.keys() != set():
        pytest.skip(f'{", ".join(required_env_vars)} are required '
                    f'to test module against Gitlab server')


@pytest.fixture
def latest_stable_version():
    tags = check_output(["git", "tag"]).splitlines()
    versions = []
    for tag in tags:
        try:
            versions.append(Version.parse(tag))
        except ValueError:
            pass
    if not versions:
        pytest.skip("No valid module versions found")
    return sorted(versions, reverse=True)[0]


@pytest.fixture(scope="session")
def gitlab():
    skip_without_gitlab()


@pytest.fixture
def module_version(tmp_path):
    @contextmanager
    def _module_version(ref):
        check_output(f"git archive {ref} | tar x -C {tmp_path}", shell=True)
        try:
            yield
        finally:
            check_output(f"rm -f {tmp_path}/*.tf", shell=True)

    return _module_version


@pytest.fixture
def root_module_path(tmp_path):
    yield tmp_path
    shutil.rmtree(tmp_path)


@pytest.fixture
def providers(root_module_path) -> Mapping[str, str]:
    return {"gitlab": "gitlabhq/gitlab"}


@pytest.fixture
def providers_file(root_module_path, providers):
    with open(Path(root_module_path, "_providers.tf.json"), "w") as f:
        json.dump({"provider": {
            provider: [] for provider in providers.keys()
        }}, f)


@pytest.fixture
def variables():
    return {}


@pytest.fixture
def var_file(root_module_path, variables):
    variables_path = Path(root_module_path, "_variables.tfvars.json")
    with open(variables_path, "w") as f:
        json.dump(variables, f)
    return variables_path


@pytest.fixture
def tf(root_module_path, providers_file, var_file):
    tf_ = tftest.TerraformTest(root_module_path)
    tf_.plan = partial(tf_.plan, tf_var_file=var_file)
    tf_.apply = partial(tf_.apply, tf_var_file=var_file)
    tf_.destroy = partial(tf_.destroy, tf_var_file=var_file)
    return tf_


@pytest.fixture
def preinstalled(tmp_path_factory, gitlab, providers):
    preinstalled_root = tmp_path_factory.mktemp("preinstalled")
    tf = tftest.TerraformTest(preinstalled_root)
    try:
        @contextmanager
        def _preinstalled(config: dict):
            config_ = {
                "provider": {provider: [] for provider in providers.keys()},
                "terraform": {
                    "required_providers": {
                        provider: {
                            "source": source
                        } for provider, source in providers.items()
                    }
                },
            }
            config_.update(config)
            with open(Path(preinstalled_root, "main.tf.json"), "w") as f:
                json.dump(config_, f)

            tf.init()
            try:
                tf.apply()
                yield tf.output()
            finally:
                tf.destroy()

        yield _preinstalled
    finally:
        shutil.rmtree(preinstalled_root)


@pytest.fixture
def plan_failure(tf, module_version):
    with module_version("HEAD"):
        tf.init()
        with pytest.raises(tftest.TerraformTestError) as e:
            tf.plan()
        yield e
