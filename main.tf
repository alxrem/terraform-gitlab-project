# Copyright 2023 Alexey Remizov <alexey@remizov.org>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

resource "gitlab_project" "default" {
  name         = var.name
  namespace_id = var.namespace_id

  description = var.description

  allow_merge_on_skipped_pipeline = var.allow_merge_on_skipped_pipeline
  analytics_access_level          = var.analytics_access_level
  approvals_before_merge          = var.approvals_before_merge
  archive_on_destroy              = var.archive_on_destroy
  archived                        = var.archived
  auto_cancel_pending_pipelines   = try(var.auto_cancel_pending_pipelines ? "enabled" : "disabled", null)
  auto_devops_deploy_strategy     = var.auto_devops_deploy_strategy
  auto_devops_enabled             = var.auto_devops_enabled
  autoclose_referenced_issues     = var.autoclose_referenced_issues
  avatar                          = var.avatar
  avatar_hash                     = var.avatar_hash
  build_git_strategy              = var.build_git_strategy
  build_timeout                   = var.build_timeout
  builds_access_level             = var.builds_access_level
  ci_config_path                  = var.ci_config_path
  ci_default_git_depth            = var.ci_default_git_depth
  ci_forward_deployment_enabled   = var.ci_forward_deployment_enabled
  ci_separated_caches             = var.ci_separated_caches

  dynamic "container_expiration_policy" {
    for_each = var.container_expiration_policy == null ? [] : [var.container_expiration_policy]
    content {
      cadence           = container_expiration_policy.value.cadence
      enabled           = container_expiration_policy.value.enabled
      keep_n            = container_expiration_policy.value.keep_n
      name_regex        = container_expiration_policy.value.name_regex
      name_regex_delete = container_expiration_policy.value.name_regex_delete
      name_regex_keep   = container_expiration_policy.value.name_regex_keep
      older_than        = container_expiration_policy.value.older_than
    }
  }

  container_registry_access_level                  = var.container_registry_access_level
  container_registry_enabled                       = var.container_registry_enabled
  default_branch                                   = var.default_branch
  emails_enabled                                   = var.emails_enabled
  environments_access_level                        = var.environments_access_level
  external_authorization_classification_label      = var.external_authorization_classification_label
  feature_flags_access_level                       = var.feature_flags_access_level
  forked_from_project_id                           = var.forked_from_project_id
  forking_access_level                             = var.forking_access_level
  group_with_project_templates_id                  = var.group_with_project_templates_id
  import_url                                       = var.import_url
  import_url_username                              = var.import_url_username
  import_url_password                              = var.import_url_password
  infrastructure_access_level                      = var.infrastructure_access_level
  initialize_with_readme                           = var.initialize_with_readme
  issues_access_level                              = var.issues_access_level
  issues_enabled                                   = var.issues_enabled
  issues_template                                  = var.issues_template
  keep_latest_artifact                             = var.keep_latest_artifact
  lfs_enabled                                      = var.lfs_enabled
  merge_commit_template                            = var.merge_commit_template
  merge_method                                     = var.merge_method
  merge_requests_enabled                           = var.merge_requests_enabled
  merge_requests_template                          = var.merge_requests_template
  merge_trains_enabled                             = var.merge_trains_enabled
  mirror                                           = var.mirror
  mirror_overwrites_diverged_branches              = var.mirror_overwrites_diverged_branches
  mirror_trigger_builds                            = var.mirror_trigger_builds
  monitor_access_level                             = var.monitor_access_level
  mr_default_target_self                           = var.mr_default_target_self
  only_allow_merge_if_all_discussions_are_resolved = var.only_allow_merge_if_all_discussions_are_resolved
  only_allow_merge_if_pipeline_succeeds            = var.only_allow_merge_if_pipeline_succeeds
  only_mirror_protected_branches                   = var.only_mirror_protected_branches
  packages_enabled                                 = var.packages_enabled
  pages_access_level                               = var.pages_access_level
  path                                             = var.path
  pipelines_enabled                                = var.pipelines_enabled
  printing_merge_request_link_enabled              = var.printing_merge_request_link_enabled
  public_builds                                    = var.public_builds
  public_jobs                                      = var.public_jobs

  dynamic "push_rules" {
    for_each = var.push_rules == null ? [] : [var.push_rules]
    content {
      author_email_regex            = push_rules.value.author_email_regex
      branch_name_regex             = push_rules.value.branch_name_regex
      commit_committer_check        = push_rules.value.commit_committer_check
      commit_message_negative_regex = push_rules.value.commit_message_negative_regex
      commit_message_regex          = push_rules.value.commit_message_regex
      deny_delete_tag               = push_rules.value.deny_delete_tag
      file_name_regex               = push_rules.value.file_name_regex
      max_file_size                 = push_rules.value.max_file_size
      member_check                  = push_rules.value.member_check
      prevent_secrets               = push_rules.value.prevent_secrets
      reject_unsigned_commits       = push_rules.value.reject_unsigned_commits
    }
  }

  releases_access_level                   = var.releases_access_level
  remove_source_branch_after_merge        = var.remove_source_branch_after_merge
  repository_access_level                 = var.repository_access_level
  repository_storage                      = var.repository_storage
  request_access_enabled                  = var.request_access_enabled
  requirements_access_level               = var.requirements_access_level
  resolve_outdated_diff_discussions       = var.resolve_outdated_diff_discussions
  restrict_user_defined_variables         = var.restrict_user_defined_variables
  security_and_compliance_access_level    = var.security_and_compliance_access_level
  shared_runners_enabled                  = var.shared_runners_enabled
  skip_wait_for_default_branch_protection = var.skip_wait_for_default_branch_protection
  snippets_access_level                   = var.snippets_access_level
  snippets_enabled                        = var.snippets_enabled
  squash_commit_template                  = var.squash_commit_template
  squash_option                           = var.squash_option
  suggestion_commit_message               = var.suggestion_commit_message
  tags                                    = var.tags
  template_name                           = var.template_name
  template_project_id                     = var.template_project_id
  topics                                  = var.topics
  use_custom_template                     = var.use_custom_template
  visibility_level                        = var.visibility_level
  wiki_access_level                       = var.wiki_access_level
  wiki_enabled                            = var.wiki_enabled

  lifecycle {
    precondition {
      condition = (
        var.container_registry_enabled == null
        || (
          var.container_registry_enabled == true
          && (var.container_registry_access_level == null || var.container_registry_access_level == "enabled")
        )
        || (
          var.container_registry_enabled == false
          && (var.container_registry_access_level == null || var.container_registry_access_level == "disabled")
        )
      )
      error_message = "Inconsistent values of var.container_registry_enabled and var.container_registry_access_level"
    }

    precondition {
      condition     = !(var.import_url != null && var.initialize_with_readme != null)
      error_message = "import_url can not be used with initialize_with_readme"
    }

    precondition {
      condition     = var.merge_trains_enabled != true || (var.merge_trains_enabled == true && var.merge_requests_enabled == true)
      error_message = "merge_trains_enabled requires merge_pipelines_enabled to be set to true to take effect."
    }

    precondition {
      condition     = var.mr_default_target_self == null || var.forked_from_project_id != null
      error_message = "mr_default_target_self requires forked_from_project_id to be set."
    }

    precondition {
      condition     = var.only_mirror_protected_branches == null || var.import_url != null
      error_message = "only_mirror_protected_branches requires import_url to be set."
    }

    precondition {
      condition     = !(var.public_jobs != null && var.public_builds != null)
      error_message = "public_jobs can not be used with public_builds"
    }

    precondition {
      condition     = !(var.template_name != null && var.template_project_id != null)
      error_message = "template_name can not be used with template_project_id"
    }
  }
}

resource "gitlab_branch_protection" "default" {
  for_each = var.protected_branches

  project = gitlab_project.default.id
  branch  = each.key

  push_access_level            = each.value.push_access_level
  merge_access_level           = each.value.merge_access_level
  allow_force_push             = each.value.allow_force_push
  code_owner_approval_required = each.value.code_owner_approval_required
}

resource "gitlab_project_variable" "default" {
  for_each = var.variables

  project = gitlab_project.default.id

  key           = each.key
  value         = each.value.value
  description   = each.value.description
  masked        = each.value.masked
  protected     = each.value.protected
  raw           = each.value.raw
  variable_type = each.value.variable_type
}

resource "gitlab_integration_pipelines_email" "default" {
  count = var.integration_pipelines_email == null ? 0 : 1

  project = gitlab_project.default.id

  recipients                   = var.integration_pipelines_email.recipients
  notify_only_broken_pipelines = var.integration_pipelines_email.notify_only_broken_pipelines
  branches_to_be_notified      = var.integration_pipelines_email.branches_to_be_notified
}

resource "gitlab_integration_telegram" "default" {
  count = var.integration_telegram == null ? 0 : 1

  project = gitlab_project.default.id

  token = var.integration_telegram.token
  room  = var.integration_telegram.room

  notify_only_broken_pipelines = var.integration_telegram.notify_only_broken_pipelines
  branches_to_be_notified      = var.integration_telegram.branches_to_be_notified
  push_events                  = var.integration_telegram.push_events
  issues_events                = var.integration_telegram.issues_events
  confidential_issues_events   = var.integration_telegram.confidential_issues_events
  merge_requests_events        = var.integration_telegram.merge_requests_events
  tag_push_events              = var.integration_telegram.tag_push_events
  note_events                  = var.integration_telegram.note_events
  confidential_note_events     = var.integration_telegram.confidential_note_events
  pipeline_events              = var.integration_telegram.pipeline_events
  wiki_page_events             = var.integration_telegram.wiki_page_events
}

data "gitlab_group" "default" {
  for_each = var.share_groups_by_paths

  full_path = each.key
}

locals {
  share_groups = merge(
    { for path, group in data.gitlab_group.default : group.id => var.share_groups_by_paths[path] },
    var.share_groups,
  )
}

resource "gitlab_project_share_group" "default" {
  for_each = local.share_groups

  project = gitlab_project.default.id

  group_id     = each.key
  group_access = each.value
}

resource "time_offset" "access_token_expiration" {
  for_each = toset([for name, token in var.access_tokens : name if token.expires_at == null])

  offset_years = 1
}

resource "gitlab_project_access_token" "default" {
  for_each = var.access_tokens

  project = gitlab_project.default.id

  name         = each.key
  scopes       = each.value.scopes
  access_level = each.value.access_level
  expires_at = try(
    formatdate("YYYY-MM-DD", time_offset.access_token_expiration[each.key].rfc3339),
    each.value.expires_at
  )
}